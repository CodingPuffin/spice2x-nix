{
  description = "Spcie2x flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  };

  inputs.flake-compat = {
    url = "github:edolstra/flake-compat";
    flake = false;
  };

  outputs = { self, nixpkgs, ... }:

    let

      version = "24-03-31";
      pname = "spice2x";
      name = "${pname}-${version}";

      supportedSystems = [ "x86_64-linux" ];

      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;

      nixpkgsFor = forAllSystems (system: import nixpkgs { inherit system; });

    in {

      # Provide some binary packages for selected system types.
      packages = forAllSystems (system:
        let pkgs = nixpkgsFor.${system};
        in {
          spice2x = pkgs.stdenv.mkDerivation {
            inherit version pname name;

            src = pkgs.fetchzip {
              url = "https://github.com/spice2x/spice2x.github.io/releases/download/${version}/${name}.zip";
              hash = "sha256-h1BC9EUbC3LFOMlVRV7J9WL85m9YW8RwSNa/pSYH+Wk=";
            };

#            sourceRoot = "${pname}/src";

            unpackPhase = ''
              echo $src
              mkdir ./out
              tar -xvf $src/src/spice2x-master.tar.gz -C ./out
              cd ./out/spice2x
            '';

            patches = [
              ./build_all.patch
              ./CMakeLists.patch
            ];

            nativeBuildInputs = [
              pkgs.wineWowPackages.stable
            ];

            buildInputs = [
#              pkgs.cmake
#              pkgs.clang
#              pkgs.cxxtools
#              pkgs.rhash
              pkgs.pkgsCross.mingwW64.rhash
              pkgs.pkgsCross.mingwW64.cmake
              pkgs.pkgsCross.mingwW64.stdenv.cc
              pkgs.pkgsCross.mingwW64.windows.pthreads
            ];

            buildPhase = ''
              chmod +x ./build_all.sh
              ./build_all.sh
            '';
          };
        });
  };
}
